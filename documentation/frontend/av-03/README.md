# Forms and dates 

## Implement the start stop (using moment.js library)

The idea is to switch the display of the **Start Work** button from the task, so that if the task has an active timer, 
it will display a **"Stop"** icon for the button, and **"Start"** icon otherwise.  

First lets include the [moment.js](https://momentjs.com/) library with the following command: 

`npm install --save moment`

This library will help us to manipulate with dates and to format them appropriately. 

### Task modifications 

In the task component we will extract the style for the **Start Work" button in a variable: 

```javascript
let workingButtonClasses = "btn-circle fa btn-success fa-play";
if (props.task.activeTimer) {
  workingButtonClasses = "btn-circle fa btn-danger fa-stop";
}
```

Then we will use this variable for the button's class: 
```
<button className={workingButtonClasses}
                  title="Start working"
                  onClick={startWorkHandler}></button>
```

### App component modifications

Now, lets include the *moment.js* library in our `App` component with the following line: 

`import Moment from 'moment';`

One more preparation. We will define the date formats in constants inside the `App.render` method: 

```javascript
const dateTimeFormat = 'DD.MM.YYYY hh:mm:ss';
const dateOnlyFormat = 'DD.MM.YYYY';
const timeOnlyFormat = 'hh:mm:ss';
```

Now, when there is an `activeTimer` for the task, we will implement a function that will stop the timer.
To do so, we add the following code: 

```javascript
this.setState((state, props) => {
    
    const stateTasksArrayRef = cloneTasks(state, index);
    const stateTask = stateTasksArrayRef[index];
    
    
    clearInterval(stateTask.activeTimer);                                         //#1
    stateTask.activeTimer = false;
    
    const startedActivity = stateTask.activity[stateTask.activity.length - 1];    //#2
    startedActivity.to = Moment().format(timeOnlyFormat);                         //#3
    
    return {tasks: stateTasksArrayRef};

});
```

The line **#1** uses the javascript native `clearInterval` function, which will terminate the timer identified with the 
`stateTask.activeTimer`.  The line **#2** obtains the last activity from the task, and sets its `to` field to the 
current time (the `Moment()` returns the current time) formatted as time only.

When there is no `activeTimer` for the task, we will modify the code by inserting a new activity that will be started 
at the moment the task start click handler is activated: 

```javascript
task.activity.push({
    date: Moment().format(dateOnlyFormat),    //#1
    from: Moment().format(timeOnlyFormat),    //#2
    to: null
});
``` 

The line **#1** displays the current moment with its date only, where the line **#2** displays only the time from the 
current moment. 


Additionally, we want to stop the timer when our `App` component will be destroyed, so we are using the `componentWillUnmount`
lifecycle hook: 

```javascript
componentWillUnmount = () => {
    console.log('unmount')
    
    this.state.tasks.forEach((t => {          // for each task
      if (t.activeTimer) {
        clearInterval(t.activeTimer);         // invoke the clearInterval for its active timer
      }         
    }));
};
```

## Adding new tasks

### TaskEditor component

The `TaskEditor` component returs a form (**#1**) containing multiple input/select fields (**#2, #3, #4**). 
The form has a handler function that is invoked when the form is submitted (when *Enter* key is clicked inside 
some of the input elements, or when the *Submit* button (**#4**) is clicked). The `name` attribute is very important
because it is used to access the corresponding input element in the `onFormSubmit` handler function. 

```
render() {
  return ( 
    <form onSubmit={onFormSubmit}>                                                  //#1
      <div className="row">
        <div className="col-md-4">
          <input name="taskName"                                                    //#2         
                 placeholder={"Enter the name of the task..."} type={"text"}
                 className="form-control"/>
        </div>
        <div className="col-md-4">                                                      
          <select name="projectName" className="form-control">                      //#3      
            <option value="Project 1">Project 1</option>
            <option value="Project 2">Project 2</option>
            <option value="Project 3">Project 3</option>
          </select>
        </div>
        <div className="col-md-4 ">

          <button type="submit"                                                     //#4
                  className="btn btn-outline-success">
            Create
          </button>

        </div>
      </div>
    </form>
  );
}  
```


The `onFormSubmit` handler function first prevents the default behaviour of the event (**#1**), which is to send a 
HTTP request to the URL provided as `action` attribute of the form. Then, we invoke the `props.onNewTask` function, which 
is provided as a propery for our component, and we create and send the task as its argument. 

We access the values of the input/select elements using `formSubmitEvent.target.{name}.value` expression form, where `{name}`
is the `name` attribute value of the certain input/select element that we want to obtain the value from. In our case, we
hard-code the client for the project for simplicity. Additionally, the new task does not have any activity and has total
time of *'00:00:00'*. 
 
```javascript
 let onFormSubmit = (formSubmitEvent) => {
    formSubmitEvent.preventDefault();                           //#1
    console.log('onFormSubmit', formSubmitEvent);

    props.onNewTask(                                            //#2
      {
        title: formSubmitEvent.target.taskName.value,
        done: false,
        project: {
          id: 1,
          name: formSubmitEvent.target.projectName.value,       //#3
          client: {
            id: 1,
            name: 'FINKI'
          }
        },
        totalTime: '00:00:00',
        activity: []
      }
    );
  };
```

### Using the TaskEditor inside the App component

Here is the import and the usage of the `TaskEditor` component: 
```
import TaskEditor from "../TaskEditor/taskEditor";
...
<TaskEditor onNewTask={onNewTask}/>
```

The `onNewTask` handler function accepts a task and it appends it at the end of the `tasks` array from the state:  
```javascript
  const onNewTask = (task) => {
      console.log('[App.js] On new task');
      this.setState((state, props) => {
        return {
          tasks: [...state.tasks, task]
        };
      });
    };
```



## Paging the tasks

### Paging using state-full TaskList component

In almost every real life application, the data arrays that are stored become large enough so that a person can not 
preview them at once. Therefore, the most common mechanism used to present meaningful amount of data, while optimizing 
the application performance at the same time, is the *paging* mechanism. This way, only a *"page"* of data is displayed
once at a time. When the user wants to get other portion of the data, he/she requests a new page. 

In order to support this mechanism, we will use an external paging component: `react-paginate`. In order to include it 
in our project, we must invoke the command: 

```npm install --save react-paginate```

Nest, we will define a state-full component named `TaskList`:  

```javascript
import React, {Component} from 'react';
import ReactPaginate from 'react-paginate';                           //#1

import Task from '../Task/task';

class TaskList extends Component {
    constructor(props) {
      super(props);
      this.state = {                                                  //#2
        pageNum: 0,
        pageSize: 2
      }
    }
  
    handlePageClick = (data) => {
      let selected = data.selected;
  
      console.log('New selected: ', selected);
      this.setState({pageNum: selected});                             //#3
    };
  
    render() {
      const offset = this.state.pageNum * this.state.pageSize;        //#4
      console.log(offset);
      const next = offset + this.state.pageSize;                      //#5
      console.log(next);
  
      const pageCount = Math.ceil(this.props.tasks.length / this.state.pageSize);   //#6
  
      let tasks = this.props.tasks                                    
        .filter((task, index) => {                                    //#7
          console.log(index)
          return index >= offset
            && index < next;
        })
        .map((task, index) => {                                      //#8
          return <Task task={task}
                       key={index}
                       startWork={this.props.startStop}
                       index={index}/>
        });
  
      return (
        <div className="col-12">
          {tasks}
          <ReactPaginate previousLabel={"previous"}                   //#9     
                         nextLabel={"next"}
                         breakLabel={<a href="">...</a>}
                         breakClassName={"break-me"}
                         pageCount={pageCount}
                         marginPagesDisplayed={2}
                         pageRangeDisplayed={5}
                         onPageChange={this.handlePageClick}
                         containerClassName={"pagination"}
                         subContainerClassName={"pages pagination"}
                         activeClassName={"active"}/>
        </div>
      );
    }
}
```

The `TaskList` component includes the `react-paginate` component at line **#1** and uses it at line **#9**. For more 
detailed description about this component, please refer to its [documentation](https://www.npmjs.com/package/react-paginate).


The `TaskList` is a state-full component since it should store and manipulate the current page and its size (**#2**). 
The `ReactPaginate` component provides a `onPageChange` attribute that accepts function which is invoked when the user 
will click on another page number. This function just changes the state with the new page number that should be displayed. 


The `render` function takes care for all the rest. It first calculates the starting (**#4**) and ending (**#5**) index 
for the current page, and how many pages should be displayed (**#6**). Next it filters only the tasks in the current page 
(**#7**) and maps them into the JSX `Task` component expression (**#8**).     


### Usng the TaskList inside the App component

In order to use the previously defined component we should import it, and then we should replace the 3 `<Task />` components 
with the `<TaskList />` component: 

```
import TaskList from "../TaskList/TaskList";

...

<TaskList tasks={this.state.tasks}
                    startStop={startWorkHandler}/>
```

We send the `this.state.tasks` that should be displayed, and the `startWorkHandler` function that should be invoked 
when the task's start/stop button is clicked. Even though the `TaskList` component do not invoke this handler, it must
get it and delegate to the `Task` component that will invoke it. 



