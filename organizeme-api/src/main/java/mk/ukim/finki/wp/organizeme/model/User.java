package mk.ukim.finki.wp.organizeme.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Riste Stojanov
 */
@Entity
public class User {

    @Id
    public String username;

    public String firstName;

    public String lastName;

    @ManyToMany
    public List<Task> tasks=new ArrayList<>();
}
