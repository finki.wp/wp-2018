package mk.ukim.finki.wp.organizeme.service.impl;

import mk.ukim.finki.wp.organizeme.model.Activity;
import mk.ukim.finki.wp.organizeme.model.Task;
import mk.ukim.finki.wp.organizeme.model.exceptions.ActivityNotFoundException;
import mk.ukim.finki.wp.organizeme.model.exceptions.DuplicateTaskException;
import mk.ukim.finki.wp.organizeme.model.exceptions.TaskNotFoundException;
import mk.ukim.finki.wp.organizeme.persistence.TaskRepository;
import mk.ukim.finki.wp.organizeme.service.CurrentTimeService;
import mk.ukim.finki.wp.organizeme.service.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author Riste Stojanov
 */
@Service
public class TaskServiceImpl implements TaskService {
    private final Logger logger = LoggerFactory.getLogger(TaskServiceImpl.class);

    private final CurrentTimeService currentTimeService;

    private final TaskRepository repository;

    public TaskServiceImpl(TaskRepository repository,
                           CurrentTimeService currentTimeService) {
        this.repository = repository;
        this.currentTimeService = currentTimeService;
    }

    @Override
    public List<Task> getAllTasks() {
        return repository.findAll();
    }

    @Override
    public Task getTaskByIndex(int index) throws TaskNotFoundException {
        return repository.getTaskById(index);
    }

    @Override
    public List<Activity> getTaskActivities(int index, Integer inLastMinutes) throws TaskNotFoundException {

        List<Task> tasks = repository.findAll();

        if (index >= tasks.size() || index < 0) {
            throw new TaskNotFoundException();
        }


        return tasks.get(index)
          .activity
          .stream()
          .filter(a -> {
              boolean result = inLastMinutes == null;

              LocalTime otherTime = currentTimeService.getCurrentTime()
                .minusMinutes(inLastMinutes);


              logger.info("from: {}, other: {}, inLastMinutes: {}", a.from, otherTime, inLastMinutes);

              return result || !a.from.isBefore(otherTime);
          })
          .collect(toList());
    }

    @Override
    public Task addNew(Task task) throws DuplicateTaskException {
        return repository.save(task);
    }

    @Override
    public Task updateTaskName(int taskIndex, String newName) throws TaskNotFoundException {
        return null;
    }

    @Override
    public Activity startActivity(int taskIndex) throws TaskNotFoundException {
        return null;
    }

    @Override
    public Activity stopActivity(int activityId) throws ActivityNotFoundException {
        return null;
    }

    @Override
    public Activity updateActivity(int activityId, LocalDateTime from, LocalDateTime to) throws ActivityNotFoundException {
        return null;
    }

    @Override
    public Task delete(int index) throws TaskNotFoundException {
        return repository.deleteById(index);
    }
}
