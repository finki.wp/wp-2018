package mk.ukim.finki.wp.organizeme.persistence;

import mk.ukim.finki.wp.organizeme.model.Task;

import java.util.List;

/**
 * @author Riste Stojanov
 */
public interface TaskRepository {


    List<Task> findAll();

    Task getTaskById(int id);

    Task save(Task task);

    Task deleteById(int id);

    Task update(int index, String title);
}
