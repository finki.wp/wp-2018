export const getTasksFromApi = () => {
  return fetch('http://api.wp-demo-2018.com/tasks/all');
};

export const createTask = (task) => {
  return fetch('http://api.wp-demo-2018.com/tasks', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      title: task.title,
      project: task.project
    })
  });
};


export const startActivity = (task) => {
  return fetch('http://api.wp-demo-2018.com/tasks/' + task.id, {
    method: 'PUT'
  });
};


export const stopActivity = (task) => {
  return fetch('http://api.wp-demo-2018.com/tasks/' + task.id, {
    method: 'PATCH'
  });
};